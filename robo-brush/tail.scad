motor_radius = 3;

$fn=64;


difference()
{
cylinder(h=12,r=motor_radius + 3,center=true);
translate([0,0,-2.6])
cylinder(h=7,r=motor_radius,center=true);
}

points = [
[0,0],[20,6],[20,12],[0,12]
];

rotate([-90,0,0])
translate([5.5,-6,0])
linear_extrude(3,center=true)
{
polygon(points);
}