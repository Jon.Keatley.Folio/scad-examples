pipe_radius = 10.5; //10;
$fn = 128;

function half(x) = x / 2;

function get_screw_holes(x,y,offset_percent) = [
		[half(x) * offset_percent, half(y) * offset_percent],
		[-half(x) * offset_percent, -half(y) * offset_percent]
	];

module child_at_points(points,depth)
{
	for(point = points)
	{
		translate([point[0],point[1],depth])
		children(0);
		
	}
}

module make_terminal_base(x,y,z,wall, lid = 3)
{
	pf = 0.7;
	
	drill_points = [
		[-half(x) * pf, half(y) * pf],
		[half(x) * pf, -half(y) * pf]
	];
	
	screw_points = get_screw_holes(x,y,pf);
	
	//main section
	difference()
	{
		cube([x + wall,y + wall,z + wall],center=true);
		
		translate([0,0,wall])
		cube([x,y,z + wall * 2],center=true);
	
		//drill holes
		child_at_points(drill_points, -(half(z) + half(wall)) + 0.01 )
		{
			cylinder(h=half(wall),r1=0.6, r2=2);
		}
	}
	
	
	

	//screw holes
	/*child_at_points(screw_points,-(half(z) - half(lid_point)) - 2)
	{
		cylinder(h=(z - lid_point) + 2,r1=2.5,r2=2);
	}*/
	
	//lid brim
	difference()
	{
		translate([0,0,half(z) - lid])
		cube([x,y,lid],center=true);
		
		translate([0,0,half(z) - lid])
		cube([x - 2,y - 2,lid + 1],center=true);
	}
	
	
}

module make_east_access(x,y,z,wall,pipe_radius)
{
	difference()
	{
		children(0);
		
		translate([half(x),0,0])
		rotate([0,90,0])
		cylinder(h=wall * 2,r=pipe_radius * 0.8,center=true);
	}
	
	difference()
	{
		translate([half(x) + half(pipe_radius) + half(wall),0,0])
		rotate([0,90,0])
		cylinder(h=pipe_radius,r=pipe_radius * 1.2,center=true);
		
		translate([half(x) + half(pipe_radius) + half(wall),0,0])
		rotate([0,90,0])
		cylinder(h=pipe_radius + 2,r=pipe_radius ,center=true);
	}
}

module make_west_access(x,y,z,wall,pipe_radius)
{
	difference()
	{
		children(0);
		
		translate([-half(x),0,0])
		rotate([0,90,0])
		cylinder(h=wall * 2,r=pipe_radius * 0.8,center=true);
	}
	
	difference()
	{
		translate([-(half(x) + half(pipe_radius) + half(wall)),0,0])
		rotate([0,90,0])
		cylinder(h=pipe_radius,r=pipe_radius * 1.2,center=true);
		
		translate([-(half(x) + half(pipe_radius) + half(wall)),0,0])
		rotate([0,90,0])
		cylinder(h=pipe_radius + 2,r=pipe_radius ,center=true);
	}
}

module make_north_access(x,y,z,wall,pipe_radius)
{
	difference()
	{
		children(0);
		
		translate([0,half(y),0])
		rotate([90,0,0])
		cylinder(h=wall * 2,r=pipe_radius * 0.8,center=true);
	}
	
	difference()
	{
		translate([0,(half(y) + half(pipe_radius) + half(wall)),0])
		rotate([90,0,0])
		cylinder(h=pipe_radius,r=pipe_radius * 1.2,center=true);
		
		translate([0,(half(y) + half(pipe_radius) + half(wall)),0])
		rotate([90,0,0])
		cylinder(h=pipe_radius + 2,r=pipe_radius ,center=true);
	}
}

module make_south_access(x,y,z,wall,pipe_radius)
{
	difference()
	{
		children(0);
		
		translate([0,-half(y),0])
		rotate([90,0,0])
		cylinder(h=wall * 2,r=pipe_radius * 0.8,center=true);
	}
	
	difference()
	{
		translate([0,-(half(y) + half(pipe_radius) + half(wall)),0])
		rotate([90,0,0])
		cylinder(h=pipe_radius,r=pipe_radius * 1.2,center=true);
		
		translate([0,-(half(y) + half(pipe_radius) + half(wall)),0])
		rotate([90,0,0])
		cylinder(h=pipe_radius + 2,r=pipe_radius ,center=true);
	}
}

module make_wall_access(x,y,z,wall,pipe_radius)
{
	difference()
	{
		children(0);
	
		translate([0,0,-(half(wall) + half(z))])
		cylinder(h=wall + 2,r=pipe_radius,center=true);
	}
}

module make_terminal_lid(x,y,z,wall, has_holes = false)
{
	pf = 0.7;
	screw_points = get_screw_holes(x,y,pf);

	union()
	{
		difference()
		{
			color("#FF00FF")
			cube([x,y,z],center=true);
			
			if(has_holes)
			{
				
				child_at_points(screw_points,0)
				{
					cylinder(h=z + 0.2 ,r1=1.5,r2=3,center=true);
				}
			}
		}
		
		difference()
		{
			color("#AA00AA")
			translate([0,0,z + 0.01])
			cube([x+wall,y+wall,z],center=true);
			
			translate([0,0,z])
			cube([x - 1,y - 1,z + 1],center=true);
		}
	}
}

//-----------------------------------------------------------------------------
module example()
{
	tx = 40;
	ty = 40;
	tz = 26;
	wall = 4;

	make_south_access(tx,ty,tz,wall,pipe_radius)
	{
		make_north_access(tx,ty,tz,wall,pipe_radius)
		{
			make_wall_access(tx,ty,tz,wall,pipe_radius)
			{
				make_west_access(tx,ty,tz,wall,pipe_radius)
				{
					make_east_access(tx,ty,tz,wall,pipe_radius)
					{
						make_terminal_base(tx,ty,tz,wall);
					}
				}
			}
		}
	}
}

module example_lid()
{
	tx = 40;
	ty = 40;
	tz = 4;
	wall = 4;

	make_terminal_lid(tx,ty,tz,wall);
}

//example_lid();

/*
TODO: move to example
tx = 40;
ty = 40;
tz = 26;
wall = 4;

show_office = false;
show_lid = false;
show_living_room = false;


if(show_office)
{
	make_wall_access(tx,ty,tz,wall,pipe_radius)
	{
			make_west_access(tx,ty,tz,wall,pipe_radius)
			{
				make_terminal_base(tx,ty,tz,wall);
			}
	}
	

}

if(show_lid)
{
	translate([0,0,tz])
	make_terminal_lid(tx,ty,3,wall);
}

if(show_living_room)
{
	make_north_access(tx,ty,tz,wall,pipe_radius)
	{
		make_wall_access(tx,ty,tz,wall,pipe_radius)
		{
				make_east_access(tx,ty,tz,wall,pipe_radius)
				{
					make_terminal_base(tx,ty,tz,wall);
				}
		}
	}
}
*/




		