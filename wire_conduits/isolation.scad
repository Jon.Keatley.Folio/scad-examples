include <terminals.scad>
v_scale = 0.08;
v_scale_inner = v_scale * 0.8;

box_width = 100;
box_height = 100;
box_depth = 40;
wall = 4;
lid = 4;

show_lid = false; 
show_box = true;

if(show_lid)
{
	union()
	{
		translate([-2,-4,0])
		linear_extrude(height=4,center=true,convexity=10)
		scale([v_scale,v_scale,1])
		import("voltage.svg", convexity = 11,center=true);

		difference()
		{
			linear_extrude(height=4,center=true,convexity=10)
			scale([v_scale,v_scale,1])
			import("voltage_border.svg", convexity = 11,center=true);

			translate([0,-2,0])
			linear_extrude(height=5,center=true,convexity=10)
			scale([v_scale_inner,v_scale_inner,1])
			import("voltage_border.svg", convexity = 11,center=true);
		}

		translate([0,0,-(lid / 2)])
		make_terminal_lid(box_width, box_height, lid, wall);
	}
}

if(show_box)
{
	translate([0,0,-(box_depth / 2) - 3 ])
	make_wall_access(box_width,box_height,box_depth,wall,box_width / 4)
	{
		make_terminal_base(box_width,box_height,box_depth,wall, lid);
	}
}


