$fn=64;

module plate(x,y,z,width,height,depth=4)
{
	half_width = width / 2;
	half_height = height / 2;
	
	rotate([0,90,0])
	{
		hull()
		{
			translate([x,y - half_width,z])
			cylinder(depth,r=half_height,center=true);
			
			translate([x,y + half_width,z])
			cylinder(depth,r=half_height,center=true);
		}
	}
}

module link(x,y,z,height,width,offset,depth=4)
{
	half_width = width / 2;
	half_height = height / 2;
	
	difference()
	{
		plate(x,y ,z + offset,height,width,depth);
		rotate([0,90,0])
		translate([x,y + width,z + offset])
		cylinder(depth + offset,r=half_height,center=true);
		
		rotate([0,90,0])
		translate([x,y - width,z + offset])
		cylinder(depth + offset,r=5,center=true);
	}
	
	difference()
	{
		plate(x,y,z,height,width,depth);
	
		rotate([0,90,0])
		translate([x,y - width,z ])
		cylinder(depth + offset,r=half_height,center=true);
	}
	
	
	rotate([0,90,0])
	translate([x,y + width,z + offset])
	cylinder(depth,r=5,center=true);
		
}

module chain_link(x,y,z,gap,length)
{
	half_gap = gap / 2;
	
	translate([x - half_gap,y,z])
	link(x,y,z,length,length / 2,-4,4);
	
	translate([x + half_gap,y,z])
	link(x,y,z,length,length / 2,4,4);
	
}


chain_link(0,0,0,60,40);