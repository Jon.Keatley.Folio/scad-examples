 include <../modules/nice_box.scad>


/*
new plan

- lid: padding section 2.8 mm for camera, screw in to secure

- body: rim for screen, tabs to screw in screen bolts - done

- handle: split connector and cylinder. connector glue on with smaller hex. cylinder screws on to connector, close cylinder


*/

function padded(x) = x + wall_size;
function half(x) = x / 2;

camera_depth = 35;
camera_width = 104.8;
camera_height = 70;
wall_size = 6;

peg_height = 31;
peg_size = 10;

bolt_padding = 0.8;
bolt_radius = 2.4 + bolt_padding;

lid_depth = 4;
top_of_lid = half(padded(camera_depth + lid_depth )) - lid_depth;
bottom_of_lid = top_of_lid - half(lid_depth);

handle_offset = 4.6;
handle_holes = [
[handle_offset + (half(padded(camera_height)) - 15),half(padded(camera_depth)) + 18],
[handle_offset - (half(padded(camera_height)) - 15) ,half(padded(camera_depth)) + 18],
];

bottom_of_case = half(padded(camera_depth));


module regular_polygon(order = 4, r=1){
     angles=[ for (i = [0:order-1]) i*(360/order) ];
     coords=[ for (th=angles) [r*cos(th), r*sin(th)] ];
     polygon(coords);
 }
 

module child_at_points(points,depth)
{
	for(point = points)
	{
		translate([point[0],point[1],depth])
		children(0);
		
	}
}

show_top = true;
show_body = false;
show_handle = false;
 
offset = 2.4;

points = [
[-half(camera_width) + offset,-half(camera_height) + offset],
[half(camera_width) - offset,-half(camera_height) + offset],
[half(camera_width) - offset,half(camera_height) - offset],
[-half(camera_width) + offset,half(camera_height) - offset]
];

//--------------------------------------------------------------------------------------------
lens_radius = 5.1;
lens_width = 16;
lens_height = 18;
lens_depth = 2;

if(show_top)
{
    translate([0,0,0.5])
    union()
    {
        difference()
        {
            color("#FF00FF")
            translate([0,0,top_of_lid ])
            cube([padded(camera_width) - (wall_size ),
                padded(camera_height) - (wall_size ) ,
                lid_depth ],center=true);

            
            //lens hole
            translate([0,0,top_of_lid ])
            cylinder(h=10,r=lens_radius,center=true,$fn=64);
            
            //peg hols
            child_at_points(points,top_of_lid)
            {
                difference()
                {
                    cylinder(h=lid_depth * 2,r2=1.2, r1=1.4,center=true,$fn=64);
                }
            }
        }
		
		//lens bolts
		lens_pad = 2.8;
		
		translate([0,0,bottom_of_lid - half(lens_pad)])
		difference()
		{
			cube([lens_width,lens_width,lens_pad],center=true);
			cylinder(h=10,r=lens_radius,center=true,$fn=64);
			
			translate([6,4,0])
			cylinder(h=lens_pad + 0.1,r=1.2,center=true,$fn=64);
			
			translate([-6,4,0])
			cylinder(h=lens_pad + 0.1,r=1.2,center=true,$fn=64);
		}
        
        //lens grip
        /*grip_width = 2;
        grip_points = [
            [-half(lens_width) - grip_width,0],
            [half(lens_width) + grip_width,0]
        ];
        
        child_at_points(grip_points,bottom_of_lid - half(lens_depth * 3))
        {
            difference() 
            {
                cube([grip_width,lens_height,lens_depth * 3],center=true);
                
                cube([grip_width + 1,lens_height - 4, lens_depth],center=true);
            }
        }*/
    }
}

//--------------------------------------------------------------------------------

screen_width = 86.4;
screen_height = 51.8;

screen_border = 3;
screen_offset = 2.8;

screen_outer_height = 59;
screen_outer_width = 99;
screen_outer_depth = 7.2;

if(show_body)
{
	cutout_width = 35;
	cutout_height = 14;
	cutout_x_offset = 51;
	cutout_z_offset = 16;
	
	rim_width = 4;

		difference()
		{
			//main box
			union()
			{
				hollow_box(
					padded(camera_width),
					padded(camera_height),
					padded(camera_depth),curve=1,wall=wall_size);
			
				//screen rim 
				translate([0,0, -(bottom_of_case - rim_width)])
				difference()
				{
					cube([camera_width,camera_height,rim_width],center=true);
					
					color("#FF0000")
					translate([-screen_offset,0, 0])
					cube([screen_outer_width,screen_outer_height,rim_width + 1],center=true);
				}
				
			}
			
			//screen hole
			translate([half(screen_offset),0, -bottom_of_case])
			cube([screen_width,screen_height,wall_size  ],center=true);
			
			//access cutout
			translate([
				half(padded(camera_width)) - cutout_x_offset,
				half(padded(camera_height)),
				half(padded(camera_depth)) - cutout_z_offset])
			cube([cutout_width,wall_size * 2,cutout_height],center=true);
		
		
			//handle bolt holes
			child_at_points(handle_holes,1.5)
			{
				color("#FFAA00")
				rotate([90,0,0])
				cylinder(h=wall_size - 2, r=bolt_radius,center=true,$fn=6);
			}
		}

		//lid grips
		child_at_points(points,bottom_of_lid - 1 )
		{
			cube([7,7,3],center=true);
		}
		
		//screen clamps
		bottom_of_rim = -(bottom_of_case -rim_width);
		
		camera_left = -half(camera_width) - half(wall_size);
		

		clamp_left_pos = camera_left + 18;
		clamp_right_pos = clamp_left_pos + 58 ;
		clamp_width = 8;
		clamp_depth = 4.4;
		clamp_height = clamp_depth + clamp_width;
		
		screen_lower_clamp = bottom_of_rim + 8.2 + half(clamp_depth);
		screen_upper_clamp = bottom_of_rim +  24.8 + half(clamp_depth);
		
		screw_radius = 2.8 / 2;
		
		clamp_lower_points = [
			[clamp_left_pos, half(camera_height) - half(clamp_height)],
			[clamp_right_pos, half(camera_height) - half(clamp_height)]
		];
		
		child_at_points(clamp_lower_points,screen_lower_clamp)
		{
			difference()
			{
				cube([clamp_width,clamp_height + 4,clamp_depth],center=true);
				
				translate([0,-clamp_depth,0])
				cylinder(h=clamp_depth + 1, r=screw_radius,center=true, $fn=128);
			}
		}
		
}
//---------------------------------------------------------------------------

if(show_handle)
{
	handle_length = 148;
	handle_radius = 28 / 2;
	handle_padding = 4;
	h_offset = 55.5;
	
	cutout_width = 38;
	cutout_height = 14;
	cutout_x_offset = 43;
	cutout_z_offset = 16;
	
	handle_z_offset = 15;
	
	//battery tube
	difference()
	{
		union()
		{
			//connection plate
			color("#00FF00")
			translate([handle_z_offset,padded(camera_height) - 26.5  ,0])
			rotate([90,0,0])
			cylinder(h=20,r1=handle_radius + 2, r2= handle_radius + 4,center=true,$fn=128);
			
			translate([handle_z_offset,padded(camera_height) + h_offset + handle_padding ,0])
			rotate([90,0,0])
			cylinder(h=handle_length + handle_padding,r=handle_radius + half(handle_padding),center=true,$fn=128);
			
			color("#00FF00")
			translate([
			handle_offset,
			half(padded(camera_height) + 5),
			half(padded(camera_depth)) - 18])
				cube([60,4,18],center=true);
		}
		
		color("#FF00FF")
		translate([handle_z_offset,padded(camera_height)  ,0])
		rotate([90,0,0])
		cylinder(h=handle_length + 112,r=handle_radius,center=true,$fn=128);
		
		color("#FF0000")
		translate([handle_offset,padded(camera_height)  ,-20])
		cube([handle_radius * 4,handle_length + 110,20],center=true);
	
		child_at_points(handle_holes,1.5)
		{
			rotate([90,0,0])
			cylinder(h=lid_depth + 10, r=1.5,center=true,$fn=64);
		}
	}
}




