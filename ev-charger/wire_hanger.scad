
half = function(x) x/2;
quarter = function(x) x/4;
double = function(x) x * 2;

handle_width = 300;
wire_guide_radius = 25;
wire_guide_flatten = 0.8;
wire_scale = 2;
handle_depth = (double(wire_guide_radius) * wire_scale) + 4;
handle_height = half((handle_width + wire_guide_radius) * wire_guide_flatten);

cutout_parcent = 0.7;
cutout_width = handle_width * cutout_parcent;
cutout_depth = handle_depth;
cutout_height = handle_height * cutout_parcent;


module cutout(w,d,h,flatten)
{
	difference()
	{
		translate([0,0,0])
		rotate([90,0,0])
		scale([1,flatten,1])
		cylinder(r=half(w ),h=d,center=true,$fn=64);
		
		translate([0,0,-half(h) + 0.5])
		cube([w * 1.2,d + 1,h],center=true);
	}
}

module jigsaw_cube(w,d,h,teeth,side,odd)
{
	tooth_depth = half(d) / teeth;
	side_offset = (side == 1?half(tooth_depth):-half(tooth_depth));
	offset = (w * side) - (half(w) + side_offset);
	range = [0:teeth - 1];
	
	padding = 0.4;

	difference()
	{
		cube([w,d,h],center=true);
		for(t = range)
		{
			d = -half(d) + 
				(double(tooth_depth) * t) + 
				half(tooth_depth) +
				(odd? tooth_depth  :0);
			translate([offset,d,0])
			cube([tooth_depth + padding,tooth_depth +padding ,h +padding],center=true);
			
		}
	}
	
	
}

module main_section()
{
	//main section
	difference()
	{
		translate([0,0,0])
		rotate([90,0,0])
		scale([1,wire_guide_flatten,1])
		cylinder(r=half(handle_width ),h=handle_depth,center=true,$fn=64);

		
		rotate([90,0,0])
		scale([1,wire_guide_flatten,wire_scale])
		rotate_extrude(angle=180, convexity=16,$fn=128)
		translate([half(handle_width),00,0])
		circle(r=wire_guide_radius,$fn=128);
		
		translate([0,0,-half(handle_height) + 0.5])
		cube([handle_width + 20,handle_depth + 1, handle_height + 1],center=true);
		
		translate([0,0, (half(handle_height) - half(cutout_height)) * 0.7])
		cutout(cutout_width,cutout_depth + 1,cutout_height, wire_guide_flatten);
	}
	
	//screw panel
	panel_height = 20;
	panel_depth = 5;
	screw_offset = 0.8;
	
	difference()
	{
		translate([0,half(handle_depth) - half(panel_depth),24])
		cube([cutout_width,panel_depth,panel_height],center=true);
		
		translate([half(cutout_width) * screw_offset,half(handle_depth) - half(panel_depth) - 1.5,24])
		rotate([-90,0,0])
		cylinder(h=panel_depth +3.5,r1=5,r2=2,center=true);
		
		translate([-(half(cutout_width) * screw_offset),half(handle_depth) - half(panel_depth) - 1.5,24])
		rotate([-90,0,0])
		cylinder(h=panel_depth +3.5,r1=5,r2=2,center=true);
	}
}



show_whole = false;
show_left = true; 
show_right = true; 

if(show_whole)
{
	main_section();
}


if(show_left)
{
	difference()
	{
		main_section();
	
		translate([quarter(handle_width),0,half(handle_height)])
		jigsaw_cube(
			half(handle_width),
			handle_depth + 1,
			handle_height,
			6,
			0,
			true
		);
	}
}

if(show_right)
{
	translate([10,0,0])
	difference()
	{
		main_section();
	
		translate([-quarter(handle_width),0,half(handle_height)])
		jigsaw_cube(
			half(handle_width),
			handle_depth + 1,
			handle_height,
			6,
			1,
			false
		);
	}
}




