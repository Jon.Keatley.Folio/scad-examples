$fn=32;


module peg_grid(peg_size=5,horizontal_gap=25,vertical_gap=55,peg_length=6,round_end=true)
{
	h = horizontal_gap - (peg_size * 2);
	v = vertical_gap - (peg_size * 2);
	h_pos = [0 ,h ,0 ,h];
	v_pos = [0 ,0 ,v ,v];
	for(i = [0:1:3])
	{
		union()
		{
			translate([h_pos[i],v_pos[i],0])
			cylinder(h=peg_length,r=peg_size,center = true);
			
			if(round_end)
			{
				translate([h_pos[i],v_pos[i],peg_length - (peg_size * 1.5)])
				sphere(r=peg_size);
			}
		}
	}
}

module slot_grid(peg_size=5,horizontal_gap=25,vertical_gap=55,peg_length=6,slot_length=4)
{
	h = horizontal_gap - (peg_size * 2);
	v = vertical_gap - (peg_size * 2);
	h_pos = [0 ,h ,0 ,h];
	v_pos = [0 ,0 ,v ,v];
	for(i = [0:1:3])
	{
		hull()
		{
			translate([h_pos[i],v_pos[i],0])
			cylinder(h=peg_length,r=peg_size,center = true);
			
			translate([h_pos[i],v_pos[i] - slot_length,0])
			cylinder(h=peg_length,r=peg_size,center = true);
			
		}
	}
}

module nice_cube(x,y,z)
{
	minkowski(){
         cube([x,y,z],center=true);
         sphere(r=2);
	}
}


module pegboard_jig(peg_size=2.5,horizontal_gap=25,vertical_gap=35)
{
	h_size = (horizontal_gap * 3) + (peg_size * 3);
	v_size = vertical_gap + (peg_size * 5);
	h_offset = (h_size / 2) - (peg_size  * 2);
	v_offset = (v_size / 2) - (peg_size * 4);
	
	peg_grid(peg_size,horizontal_gap,vertical_gap,peg_length=8,round_end=true);
	
	difference()
	{
		translate([h_offset,v_offset,-(peg_size * 1.5)])
		nice_cube(h_size,v_size,peg_size * 2);
		

		translate([(horizontal_gap * 2) - peg_size,0,-(peg_size)])
		slot_grid(peg_size,horizontal_gap,vertical_gap,peg_length=20,slot_length=4);
	}
	
	
}

pegboard_jig();