
$fn=64;

wall_width = 1.5;
half_width = wall_width / 2;

holder_width = 180;
holder_height = 230;
holder_depth = 105;

shelf_angle = 45;
shelf_depth = (holder_depth / sin(shelf_angle)) - wall_width;
shelf_height = holder_depth / tan(shelf_angle);

module wedge(x,y,z)
{
	difference()
	{
		cube([x,y,z],center=true);
		
		translate([-(x/4),0,(z/4)])
		rotate([0,-shelf_angle,0])
		cube([x * 2 ,y + 2,z * 3],center=true);
	}
}

difference()
{
union()
{
difference()
{
	union()
	{
		translate([0,-(holder_width / 2)+ half_width,0])
		cube([holder_depth,wall_width,holder_height],center=true);

		translate([0,(holder_width / 2) - half_width,0])
		cube([holder_depth,wall_width,holder_height],center=true);

		translate([-(holder_depth / 2) + half_width,0,0])
		cube([wall_width,holder_width,holder_height], center=true);

		translate([0,0,-(holder_height / 2) + half_width])
		cube([holder_depth,holder_width,wall_width],center=true);

		difference(){
		cube([holder_depth,wall_width,holder_height],center=true);
		translate([shelf_depth / 2,0,((holder_height * 0.52) - (3 * (shelf_height * 0.6))) - shelf_depth / 4])
		rotate([0,-shelf_angle,0])
		cube([shelf_depth * 2,holder_width,shelf_depth * 1.1],center=true);
		}

		for(a = [1:1:3])
		{
			translate([0,0,(holder_height * 0.55) - (a * (shelf_height * 0.6))])
			rotate([0,-shelf_angle,0])
			cube([shelf_depth,holder_width,wall_width],center=true);
		}
	}

	difference()
	{
		translate([holder_depth / 2,0,(holder_height/2)])
		cube([holder_depth / 2,holder_width + 2,holder_depth / 2],center=true);
		translate([holder_depth / 4,0,(holder_height/2) - (holder_depth / 4)])
		rotate([90,0,0])
		cylinder(h=holder_width + 4,r = holder_depth / 4,center=true);
	}
}

translate([0,0,-(holder_height / 2) + (shelf_depth / 8)])
wedge(holder_depth,holder_width,shelf_depth / 4);
}

translate([0,0,-(holder_height / 2) - 10])
cube([holder_depth,holder_width + 2,20],center=true);
}
