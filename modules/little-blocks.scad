//targetX2 = 15.8; // 8 diff
//targetX3 = 23.8;
//targetX4 = 31.8;


new_dot = 3.6 / 2;

//dot_height = settings[0];
//dot_gap = settings[1];
//dot_radius = settings[2];
//block_height = settings[3];
//edge_gap = settings[4];


function standard_size() =
[
	1.8, 
	8,
	2.46,
	3,
	0.2
];
//29.8
function mini_size() =
[
	1.5,
	6,
	3.6 / 2,
	2.4,
	0.1
];


function dot_length(dots, dot_gap=dot_gap) = dots * dot_gap;
function half(value) = value / 2;

module dot_matrix(
		dots_x,
		dots_y,
		dot_height=dot_height,
		dot_gap=dot_gap,
		dot_radius=dot_radius)
{
	offset_x = half(dot_gap) -half(dot_length(dots_x));
	offset_y = half(dot_gap) -half(dot_length(dots_y));
	
	translate([offset_x,offset_y,0])
	union()
	{
		for( y = [0:dots_y -1])
		{
			for( x = [0:dots_x -1])
			{
				translate([x * dot_gap,y * dot_gap,0])
				cylinder(h=dot_height,r=dot_radius,center=true);
			}

		}
	}
}

module small_dots(
		dots_x,
		dots_y,
		dot_height=dot_height,
		dot_gap=dot_gap,
		dot_radius=dot_radius,
		block_height = block_height
		)
{
	mod_x = dots_x > 1 ? half(dot_gap) : 0;
	mod_y = dots_y > 1 ? half(dot_gap) : 0;
	
	offset_x = (half(dot_gap) - half(dot_length(dots_x))) + mod_x;
	offset_y = (half(dot_gap) - half(dot_length(dots_y))) + mod_y;
	
	end_x = max(dots_x - 2,0);
	end_y = max(dots_y - 2,0);

	translate([offset_x,offset_y,-half(block_height) - half(dot_height)])
	union()
	{
		for( y = [0:end_y])
		{

			for( x = [0:end_x])
			{
				translate([x * dot_gap,y * dot_gap,0])
				cylinder(h=block_height,r=dot_radius,center=true);
			}

		}
	}
}

module dot_block(dots_x,dots_y,dot_gap = dot_gap, block_height = block_height, edge_gap=edge_gap)
{
	hg = edge_gap;
	echo(dot_length(dots_x) - hg);
	cube([dot_length(dots_x, dot_gap) - hg,dot_length(dots_y, dot_gap) - hg,block_height],center=true);
	
}

module dot_base(dots_x,dots_y)
{
	union()
	{
		dot_matrix(dots_x,dots_y, $fn=128);
		translate([0,0,-half(block_height) - half(dot_height)])
		dot_block(dots_x,dots_y);
	}
}

module brick(dots_x, dots_y)
{
	difference()
	{
		dot_base(dots_x, dots_y);
	
		color("#FF00FF")
		translate([0,0,-half(block_height) - (dot_height - 0.4)])
		dot_block(dots_x, dots_y, dot_gap, dot_height + 0.22, dot_gap - (dot_radius * 2) );
	}
		
	if( dots_x == 1 || dots_y == 1)
	{
		//small dots
		echo("small");
		small_dot_radius = half(dot_gap - (dot_radius *2));
		small_dots(dots_x, dots_y, dot_height, dot_gap, small_dot_radius, block_height, $fn=128);
	}
	else
	{
		//big dots
		echo("big");
	}

	
}

settings = standard_size();
dot_height = settings[0];
dot_gap = settings[1];
dot_radius = settings[2];
block_height = settings[3];
edge_gap = settings[4];

brick(5,1);