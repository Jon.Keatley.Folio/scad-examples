//$fn=64;

module nice_box(x=10,y=10,z=10,curve=1)
{
	nx = x - curve;
	ny = y - curve;
	nz = z - curve;
    if(curve > 0)
    {
        minkowski(){
            cube([nx,ny,nz],center=true);
            sphere(r=curve);
        }
    }
    else
    {
        cube([x,y,z],center=true);
    }
}

module hollow_box(x=10,y=10,z=10,curve=1,wall=1)
{
	nx = x - wall;
	ny = y - wall;
	nz = z + wall;
	difference()
	{
		nice_box(x,y,z,curve);
		translate([0,0,wall])
			nice_box(nx,ny,nz,curve);
	}
}

//hollow_box(x=10,y=20,z=8,curve=0.05,wall = 2);

//cube([10,20,8]);