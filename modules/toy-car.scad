$fa = 1;
$fs = 0.4;

//main body

difference() {
difference() {
difference() {
cube([60,20,10],center=true);
translate([20,0,0])
    rotate([90,0,0])
        cylinder(h=30,r=2.6,center=true);
}
translate([-20,0,0])
    rotate([90,0,0])
        cylinder(h=30,r=2.6,center=true);
}



translate([-31,0,7])
rotate([0,45,0])
    cube([10,22,15],center=true);
}



difference(){
translate([5,0,10 - 0.001])
    cube([30,20,10],center=true);

translate([-10,0,10])
rotate([0,25,0])
    cube([10,22,20],center=true);
}

 
//front wheels
translate([-20,-15,0])
    rotate([90,0,0])
        cylinder(h=3,r=8,center=true);
        
translate([-20,15,0])
    rotate([90,0,0])
        cylinder(h=3,r=8,center=true);

translate([-20,0,0])
    rotate([90,0,0])
        cylinder(h=30,r=2,center=true);

//back wheels

translate([20,-15,0])
    rotate([90,0,0])
        cylinder(h=3,r=8,center=true);
        
translate([20,15,0])
    rotate([90,0,0])
        cylinder(h=3,r=8,center=true);
      
translate([20,0,0])
    rotate([90,0,0])
        cylinder(h=30,r=2,center=true);