pipe_radius = 40 / 2;
inner_pipe_radius = 33 / 2;
pipe_thickness = 3.12;

function half(x) = x/2;
function double(x) = x * 2;

module pipe_extension(radius, thickness, inner_length, length, oring_inset)
{
	overlap = 5;
	ir = radius - thickness;
	translate([0,0,half(length + inner_length) - overlap])
	difference()
	{
		cylinder(h=inner_length + overlap, r = ir ,center=true,$fn=128);
		cylinder(h=inner_length + overlap + 1, r2 = ir - thickness, r1 = ir ,center=true,$fn=128);
	
		translate([0,0,overlap])
		if(oring_inset > 0)
		{
			difference()
			{
				cylinder(h=oring_inset, r = ir + 0.1   ,center=true,$fn=128);
				cylinder(h=oring_inset + 1, r = ir - half(oring_inset)    ,center=true,$fn=128);
			}
		}
	}
	
	
	difference()
	{
		cylinder(h=length + overlap, r = radius ,center=true,$fn=128);
		cylinder(h=length + overlap + 1, r = radius - thickness,center=true,$fn=128);
	}
	
}


pipe_extension(
	pipe_radius,
	pipe_thickness,
	20,
	20,
	2
);