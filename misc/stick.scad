
function half(x) = x/2;
function quarter(x) = x/4;

module stick(x,y,z, lip)
{
	padding = 0.2;
	difference()
	{
		cube([x,y,z],center=true);
		
		translate([0,-half(y) + half(lip),quarter(z)])
		cube([x + padding,lip + padding,half(z) + padding],center=true);
		
		translate([0,half(y) - half(lip),-quarter(z)])
		cube([x + padding,lip + padding,half(z) + padding],center=true);
	}
	
}


stick(6,200,6, 10);