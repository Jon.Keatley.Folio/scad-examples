standard_door_depth = 35.6;
thickness = 2;

module door_hanger(x,y,z,door_depth = standard_door_depth, reenfource=false)
{
	door_depth = door_depth + thickness;
	half_thickess = thickness / 2;
	cube([x,thickness,z],center=true);
	translate([0,-y / 2,(-z / 2) + half_thickess])
	cube([x,y,thickness],center=true);
	
	translate([0,door_depth / 2,(z / 2) - half_thickess])
	cube([x,door_depth,thickness],center=true);
}

door_hanger(20,30,50);