function half(x) = x / 2;

module wall_spacer(wall_radius, surface_radius, gap)
{
	$fn=16;
	hollow = min(half(gap),4);
	cylinder(hollow,r=surface_radius,center=true);
	
	translate([0,0,hollow - half(half(gap))])
	cylinder(half(gap),r=wall_radius,center=true);
}


board = 3.2;
gap = 8.8;


wall_spacer(15,20,gap - board);