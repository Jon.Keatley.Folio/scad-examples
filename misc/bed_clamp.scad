
function half(x) = x / 2;

function middle() = [
1.6, //padding
40, //bed_face
30, //bed_adj
];

function end() = [
1, //padding
29, //bed_face
39, //bed_adj
];



target_vals = end();
padding = target_vals[0];
bed_face = target_vals[1] + padding;
bed_adj = target_vals[2] + padding;

wall = 4;
bed_lip = 57 + padding;
bed_wall = 26 + padding;

rail_height = 0.2 * 4;

gap = bed_lip + bed_wall + bed_adj + (wall * 1.4);

block = 20;
hblock = half(block);

bottom = half(bed_adj);

//block
translate([gap - hblock - half(wall),0, -bottom + hblock])
cube([block,bed_face + wall,block],center=true);


//rail
translate([half(gap),0,-(half(bed_adj) - half(rail_height))])
cube([bed_lip + bed_wall + (wall * 1.3),bed_face + wall,rail_height],center=true);

//strength
translate([half(bed_adj + wall),0,-(half(bed_adj) - half(rail_height))])
rotate([0,-90,-90])
linear_extrude(bed_face + wall,center=true)
polygon([
[0,0],
[4,0],
[0,wall * 4],
[0,0]
]);

//holder
//translate([0,0,-half(bed_adj)])
difference()
{
cube([bed_adj + wall,bed_face + wall,bed_adj],center=true);
cube([bed_adj,bed_face,bed_adj + 1],center=true);
}