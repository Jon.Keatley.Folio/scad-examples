motor_radius = 11;
motor_height = 17;
cage_length = 16;
motor_width = motor_radius * 2.2;

$fn=64;

translate([0,0,4.5])
union()
{
	translate([0,0,motor_height - 4 ])
	cube([motor_width,cage_length,2],center=true);
	difference()
	{
	translate([0,0,4])
	cube([motor_width,cage_length,motor_height],center=true);
	translate([0,0,3]) rotate([90,0,0])
	cylinder(h=cage_length + 8,r=motor_radius,center=true);
	}
}

nut_end = (motor_width / 2) + 3.5;

echo("nut_end is",nut_end * 2);

difference()
{
translate([ nut_end,0,1])
cube([7,8,2],center=true);

translate([nut_end,0,1])
cylinder(h=3,r1=2,r2=2.5,center=true);
}

difference()
{
translate([ -nut_end,0,1])
cube([7,8,2],center=true);

translate([ -nut_end,0,1])
cylinder(h=3,r1=2,r2=2.5,center=true);
}

color("#FF00FF")
cube([nut_end * 2,0.5,0.5],center=true);

