include <../motor-cage.scad>

//holds 4 pens 
//holds 2 AA batteries
//central motor
//motor paddle
// on off switch
/*
	clips for four pens 
	two sections
		- top holds motor
		- bottom holds battery pack
		
*/
size = 4;

pen_padding = 0.1;
pen_radius = 4 + pen_padding;
padded_pen_radius = pen_radius + 1;
pen_length = 100;

battery_depth = 60;
battery_width = 33;
case_radius = 80 / 2;

motor_radius = 11;
motor_height = 17;
cage_length = 16;
motor_width = motor_radius * 2.2;

switch_width = 12;
switch_depth = 19;

function half (h) = h / 2;

//motor_case();

//top
difference()
{
	union()
	{
		difference()
		{
			translate([0,0,half(pen_length)])
			cylinder(h=4,r=case_radius,center=true);
			
			translate([0,0,half(pen_length)])
			cylinder(h=6,r=case_radius / 1.5,center=true);
		}

		//motor link 
		translate([motor_radius + 6.5,0, half(pen_length)])
		cube([size + 14.5,case_radius * 1.4,size],center=true);
	}

	translate([switch_width * 2,0,half(pen_length)])
	cube([switch_width,switch_depth,size + 2],center=true);
}


translate([-4,0,half(pen_length) - 6])
rotate([90,0,90])
union()
{
basic_motor_case(0,0,0);
}

//bottom
translate([0,0,-half(pen_length)])
cylinder(h=4,r=case_radius,center=true);



//struts
points = [
	
	[0,case_radius - half(size)],
	[-(case_radius - half(size)),0],
	[0,-(case_radius - half(size))],
	[case_radius - half(size),0],
];

pf = padded_pen_radius + 1;
offsets = [
	[0, pf],
	[-pf,0],
	[0, -pf],
	[pf,0],
];

rotate([0,0,45])
union()
{
for(i = points)
{
	translate([i[0],i[1],0])
	cube([size,size,pen_length],center=true);
}
}

//pen clips
for(i = [0:3])
{
	translate([points[i][0] + offsets[i][0],points[i][1] + offsets[i][1],half(pen_length) - 1])
	rotate([0,0,i * 90])
	difference()
	{
		cylinder(h=6,r=padded_pen_radius,center=true);
		translate([0,0,-1])
		cylinder(h=4.5,r=pen_radius,center=true);
		
		translate([0,pen_radius,-1])
		cube([pen_radius * 1.5,11,4.5],center=true);
	}
}

for(i = [0:3])
{
	translate([points[i][0] + offsets[i][0],points[i][1] + offsets[i][1],-(half(pen_length) - 1)])
	rotate([0,0,i * 90])
	difference()
	{
		cylinder(h=6,r=padded_pen_radius,center=true);
		cylinder(h=7,r=pen_radius,center=true);
		
		translate([0,pen_radius,0])
		cube([pen_radius * 1.5,11,7],center=true);
	}
}

